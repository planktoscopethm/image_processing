use opencv::{
    core::{no_array, KeyPoint, Point, Scalar, Vector, CV_8UC1},
    features2d::{AKAZE_DescriptorType, KAZE_DiffusivityType, AKAZE},
    imgcodecs, imgproc,
    prelude::*,
    Result,
};
use plancto_image::{add_suffix, ensure_output_directory, read_images};
use rayon::iter::{IntoParallelIterator, ParallelIterator};

/// Applies AKAZE feature detection to an image, filters keypoints by size, and creates a mask highlighting the filtered keypoints.
///
/// This function performs the following steps:
/// 1. Reads the input image.
/// 2. Creates an AKAZE detector with specific parameters.
/// 3. Detects keypoints in the image.
/// 4. Filters keypoints based on their size (larger than a threshold).
/// 5. Creates a mask image filled with black pixels.
/// 6. Iterates through the filtered keypoints:
///    - Converts each keypoint to a Point.
///    - Draws a white filled circle on the mask image centered at the Point with a specified radius.
/// 7. Creates an output image with the same size and type as the input image.
/// 8. Copies the original image to the output image only where the mask is non-zero (white pixels).
/// 9. Saves the output image with highlighted keypoints.
///
/// # Arguments
///
/// * `origin_path`: Path to the input image (as a `&str`).
/// * `destination_path`: Path to save the output image with highlighted keypoints (as a `&str`).
///
/// # Returns
///
/// A `Result` indicating success or failure.
fn apply_akaze(origin_path: &str, destination_path: &str) -> Result<()> {
    // Read the image
    let img = imgcodecs::imread(origin_path, imgcodecs::IMREAD_COLOR)?;

    // Create AKAZE detector
    let mut akaze = AKAZE::create(
        AKAZE_DescriptorType::DESCRIPTOR_MLDB,
        0,
        3,
        0.005f32,
        4,
        4,
        KAZE_DiffusivityType::DIFF_PM_G1,
        -1,
    )?;

    // Detect keypoints
    let mut keypoints: Vector<KeyPoint> = Vector::new();
    akaze.detect(&img, &mut keypoints, &no_array())?;
    let mut mask = Mat::new_rows_cols_with_default(
        img.rows(),
        img.cols(),
        CV_8UC1,
        Scalar::new(0.0, 0.0, 0.0, 0.0),
    )?;
    keypoints
        .clone()
        .iter()
        .filter(|k| k.size() > 8.0)
        .map(|kp| {
            println!("{:?}", kp);
            Point::new(kp.pt().x as i32, kp.pt().y as i32)
        })
        .for_each(|center| {
            imgproc::circle(
                &mut mask,
                center,
                100, // 100 pixel radius
                Scalar::new(255.0, 255.0, 255.0, 0.0),
                -1, // Filled circle
                imgproc::LINE_8,
                0,
            )
            .unwrap();
        });
    let mut output = Mat::new_rows_cols_with_default(
        img.rows(),
        img.cols(),
        img.typ(),
        Scalar::new(255.0, 255.0, 255.0, 0.0),
    )?;
    // Copy the original image to the output only where the mask is non-zero
    img.copy_to_masked(&mut output, &mask)?;
    // Save the output image
    imgcodecs::imwrite(destination_path, &output, &Vector::new())?;

    Ok(())
}
fn filtered_akaze(source_path: &str) {
    let images: Vec<String> = read_images(source_path, "Jpeg").expect("nof found");
    let feature_path: String =
        ensure_output_directory(&add_suffix(source_path, "filtered").unwrap()).unwrap();
    images
        .into_par_iter()
        .map(|img| {
            println!("appling akaze on {}", img);
            (
                format!("{}{}.Jpeg", source_path, img),
                format!("{}{}.Jpeg", feature_path, img),
            )
        })
        .for_each(|(src, des)| apply_akaze(&src, &des).unwrap());
}
fn main() {
    [
        "Gießen_Wieseck_1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-2",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-3",
    ]
    .into_iter()
    .for_each(|folder| filtered_akaze(format!("./data/{}_processed/", folder).as_str()));
}
