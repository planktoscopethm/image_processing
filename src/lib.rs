use image::RgbImage;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::{
    fs,
    io::ErrorKind,
    path::{Path, PathBuf},
};
/// Reads all JPG images from a given folder.
///
/// This function reads all image files with the specified codec from a directory and returns their paths as a `Vec<String>`.
/// It uses Rayon for parallel processing.
///
/// # Arguments
///
/// * `folder_path`: The path to the folder containing the images (as a `&str`).
/// * `codec`: The image codec (e.g., "jpg", "png") to filter for (as a `&str`).
///
/// # Returns
///
/// A `Result` containing a vector of image paths as strings if successful, or an `ErrorKind` if an error occurs while reading the directory or the files are not valid images.
pub fn read_images(folder_path: &str, codec: &str) -> Result<Vec<String>, ErrorKind> {
    match fs::read_dir(folder_path) {
        Ok(dir) => {
            let paths: Vec<PathBuf> = dir
                .map(|entry| entry.expect("not a valid entry").path())
                .filter(|path| path.is_file() && path.extension().map_or(false, |ext| ext == codec))
                .collect::<Vec<PathBuf>>();
            println!("Read {} images.", paths.len());
            Ok(paths
                .into_par_iter()
                .map(|img| {
                    img.to_str()
                        .unwrap()
                        .to_string()
                        .replace(format!(".{}", codec).as_str(), "")
                        .replace(folder_path, "")
                })
                .collect::<Vec<String>>())
        }
        _ => Err(ErrorKind::InvalidInput),
    }
}

/// Ensures the output directory exists and creates it if necessary.
///
/// This function checks if the provided path exists as a directory. If it does, it removes any existing content (with appropriate error handling). If it doesn't exist, it creates the directory structure.
///
/// # Arguments
///
/// * `input`: The path to the output directory (as a `&str`).
///
/// # Returns
///
/// A `Result` containing the output directory path as a `String` if successful, or an `ErrorKind` indicating a permission error if creation failed.
pub fn ensure_output_directory(input: &str) -> Result<String, ErrorKind> {
    let path = Path::new(input);

    if path.exists() {
        fs::remove_dir_all(path).map_err(|_| ErrorKind::PermissionDenied)?;
    }

    fs::create_dir_all(path)
        .inspect(|_x| println!("Folder Path:{} is valid", input))
        .map(|_| input.to_string())
        .map_err(|_| ErrorKind::PermissionDenied)
}

/// Opens an image file and converts it to an `RgbImage`.
///
/// This function attempts to open an image file located at the specified path and folder. If successful, it converts the loaded image to an `RgbImage` format.
///
/// # Arguments
///
/// * `folder_path`: The path to the folder containing the image (as a `&str`).
/// * `file_path`: The filename of the image within the folder (as a `&str`).
///
/// # Returns
///
/// A `Result` containing an `RgbImage` if successful, or an `ErrorKind::InvalidInput` if the file cannot be opened or converted.
pub fn path_to_rgb_image(folder_path: &str, file_path: &str) -> Result<RgbImage, ErrorKind> {
    println!("Opening {}", file_path);
    match image::open(format!("{}{}.jpg", folder_path, file_path)) {
        Ok(img) => Ok(img.to_rgb8()),
        Err(_) => Err(ErrorKind::InvalidInput),
    }
}

/// Constructs a new folder path based on the input path and a temporary path.
///
/// This function takes an input path (assumed to be a file path) and a temporary path. It extracts the folder structure from the input path (assuming it has a specific format) and combines it with the temporary path to create a new folder path.
///
/// # Arguments
///
/// * `input`: The input path (as a `&str`).
/// * `temp_path`: The temporary path to be used for the new folder (as a `&str`).
///
/// # Returns
///
/// A `Result` containing the constructed folder path as a `String` if the input path has the expected format, or an `ErrorKind::InvalidInput` otherwise.
pub fn add_folder(input: &str, temp_path: &str) -> Result<String, ErrorKind> {
    let parts: Vec<String> = input.rsplitn(3, '/').map(|part| part.to_string()).collect();
    match parts.len() == 3 {
        true => Ok(format!(
            "{}/{}/{}/",
            parts[2],
            temp_path,
            parts[1].trim_end_matches('/')
        )),
        _ => Err(ErrorKind::InvalidInput),
    }
}

/// Appends a suffix to an input path.
///
/// This function takes an input path (assumed to be a file path) and a suffix string. It extracts the folder structure from the input path (assuming it has a specific format) and combines it with the suffix to create a new path.
///
/// # Arguments
///
/// * `input`: The input path (as a `&str`).
/// * `suffix`: The suffix string to be appended (as a `&str`).
///
/// # Returns
///
/// A `Result` containing the constructed path with the suffix appended as a `String` if the input path has the expected format, or an `ErrorKind::InvalidInput` otherwise.
pub fn add_suffix(input: &str, suffix: &str) -> Result<String, ErrorKind> {
    let parts: Vec<String> = input.rsplitn(3, '/').map(|part| part.to_string()).collect();
    match parts.len() == 3 {
        true => Ok(format!(
            "{}/{}{}/",
            parts[2],
            parts[1].trim_end_matches('/'),
            suffix
        )),
        _ => Err(ErrorKind::InvalidInput),
    }
}
