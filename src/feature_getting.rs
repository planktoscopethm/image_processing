use opencv::{
    core::{no_array, KeyPoint, Scalar, Vector},
    features2d::{self, AKAZE_DescriptorType, KAZE_DiffusivityType, AKAZE},
    imgcodecs,
    prelude::*,
    Result,
};
use plancto_image::{add_suffix, ensure_output_directory, read_images};
use rayon::iter::{IntoParallelIterator, ParallelIterator};
/// Applies AKAZE feature detection to an image and saves the result with keypoints drawn.
///
/// # Arguments
/// * `origin_path`: Path to the input image.
/// * `destination_path`: Path to save the output image with keypoints.
///
/// # Returns
/// A `Result` indicating success or failure.
fn apply_akaze(origin_path: &str, destination_path: &str) -> Result<()> {
    // Read the image
    let img = imgcodecs::imread(origin_path, imgcodecs::IMREAD_COLOR)?;

    // Create AKAZE detector
    let mut akaze = AKAZE::create(
        AKAZE_DescriptorType::DESCRIPTOR_MLDB,
        0,
        1,
        0.01f32,
        4,
        4,
        KAZE_DiffusivityType::DIFF_PM_G1,
        70,
    )?;

    // Detect keypoints
    let mut keypoints: Vector<KeyPoint> = Vector::new();
    akaze.detect(&img, &mut keypoints, &no_array())?;
    keypoints = keypoints
        .clone()
        .iter()
        .map(|k| {
            println!("{:?}", k);
            k
        })
        .collect::<Vector<KeyPoint>>();
    // Draw keypoints on the image
    let mut img_with_keypoints = img.clone();
    features2d::draw_keypoints(
        &img,
        &keypoints,
        &mut img_with_keypoints,
        Scalar::new(0.0, 255.0, 0.0, 0.0), // Green color
        features2d::DrawMatchesFlags::DRAW_RICH_KEYPOINTS,
    )?;

    // Save the image with keypoints
    imgcodecs::imwrite(destination_path, &img_with_keypoints, &Vector::new())?;

    Ok(())
}
fn akaze_feature(source_path: &str) {
    let images: Vec<String> = read_images(source_path, "Jpeg").expect("nof found");
    let feature_path: String =
        ensure_output_directory(&add_suffix(source_path, "feature").unwrap()).unwrap();
    images
        .into_par_iter()
        .map(|img| {
            println!("appling akaze on {}", img);
            (
                format!("{}{}.Jpeg", source_path, img),
                format!("{}{}.Jpeg", feature_path, img),
            )
        })
        .for_each(|(src, des)| apply_akaze(&src, &des).unwrap());
}
fn main() {
    ["Gießen_Wießeck"]
        .into_iter()
        .for_each(|path| akaze_feature(format!("./data/{}_processed/", path).as_str()));
}
