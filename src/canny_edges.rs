use opencv::{
    core::{
        convert_scale_abs, normalize, Mat, Point, Scalar, Vector, BORDER_DEFAULT, CV_16S, CV_8U,
        NORM_MINMAX,
    },
    imgcodecs,
    imgproc::{
        canny, cvt_color, draw_contours, find_contours, laplacian, LineTypes::LINE_8,
        CHAIN_APPROX_SIMPLE, COLOR_BGR2GRAY, RETR_EXTERNAL,
    },
    Result,
};
use plancto_image::{add_suffix, ensure_output_directory, read_images};
use rayon::iter::{IntoParallelIterator, ParallelIterator};

fn apply_canny_and_laplacian(origin_path: &str, destination_path: &str) -> Result<()> {
    let mut img: Mat = imgcodecs::imread(origin_path, imgcodecs::IMREAD_COLOR)?;
    let mut edges: Mat = Mat::default();
    let mut gray: Mat = Mat::default();
    let mut sharpened: Mat = Mat::default();

    // Convert to grayscale
    cvt_color(&img, &mut gray, COLOR_BGR2GRAY, 0)?;

    // Apply Laplacian sharpening
    laplacian(&gray, &mut sharpened, CV_16S, 3, 1.0, 0.0, BORDER_DEFAULT)?;
    let mut abs_dst = Mat::default();
    convert_scale_abs(&sharpened, &mut abs_dst, 1.0, 0.0)?;
    normalize(
        &abs_dst,
        &mut sharpened,
        0.0,
        255.0,
        NORM_MINMAX,
        CV_8U,
        &Mat::default(),
    )?;

    // Apply Canny edge detection
    canny(&sharpened, &mut edges, 100.0, 200.0, 3, false)?;

    // Find contours
    let mut contours = Vector::<Vector<Point>>::new();
    find_contours(
        &edges,
        &mut contours,
        RETR_EXTERNAL,
        CHAIN_APPROX_SIMPLE,
        Point::default(),
    )?;

    // Draw contours
    draw_contours(
        &mut img,
        &contours,
        -1,
        Scalar::new(70.0, 70.0, 70.0, 70.0),
        1,
        LINE_8.into(),
        &Mat::default(),
        i32::MAX,
        Point::default(),
    )?;

    imgcodecs::imwrite(destination_path, &img, &Vector::new())?;

    Ok(())
}

fn canny_and_laplacian_feature(source_path: &str) {
    let feature_path: String =
        ensure_output_directory(&add_suffix(source_path, "_canny_laplacian").unwrap()).unwrap();

    read_images(source_path, "Jpeg")
        .expect("not found")
        .into_par_iter()
        .map(|img| {
            println!("applying canny and laplacian on {}", img);
            (
                format!("{}{}.Jpeg", source_path, img),
                format!("{}{}.Jpeg", feature_path, img),
            )
        })
        .for_each(|(src, des)| apply_canny_and_laplacian(&src, &des).unwrap());
}

fn main() {
    [
        "Gießen_Wieseck_1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-2",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-3",
    ]
    .into_iter()
    .for_each(|path| {
        canny_and_laplacian_feature(format!("./data/{}_processedfiltered/", path).as_str())
    });
}
