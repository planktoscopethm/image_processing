use image::{self, GenericImageView, ImageBuffer, Rgb, RgbImage};
use plancto_image::{add_suffix, ensure_output_directory, path_to_rgb_image, read_images};
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
use std::sync::atomic::{AtomicU64, Ordering};
/// Calculates the average pixel value of a vector of images.
///
/// # Arguments
///
/// * `image_files`: The [`Vec<String>`](https://doc.rust-lang.org/std/vec/struct.Vec.html) of files_paths to average.
/// * `folder_path`: The [&str](https://doc.rust-lang.org/std/primitive.str.html) folder_path containing the images.
///
/// # Returns
///
/// An [`RgbImage`](https://docs.rs/image/latest/image/type.RgbImage.html#) representing the average of the input images.
fn average_pic(image_files: Vec<String>, folder_path: &str) -> RgbImage {
    let (width, height) = match image::open(format!("{}{}.jpg", folder_path, &image_files[0])) {
        Ok(img) => img.dimensions(),
        Err(err) => panic!("Failed to open first image{}", err),
    };
    println!("{}x{}", width, height);
    let length = image_files.len() as u64;
    let sum_buffer: Vec<(AtomicU64, AtomicU64, AtomicU64)> = (0..(width * height) as usize)
        .map(|_| (AtomicU64::new(0), AtomicU64::new(0), AtomicU64::new(0)))
        .collect();
    image_files.par_iter().for_each(|file_path| {
        path_to_rgb_image(folder_path, file_path)
            .unwrap()
            .enumerate_pixels()
            .for_each(|(x, y, pixel)| {
                let idx = (y * width + x) as usize;
                sum_buffer[idx]
                    .0
                    .fetch_add(u64::from(pixel[0]), Ordering::Relaxed);
                sum_buffer[idx]
                    .1
                    .fetch_add(u64::from(pixel[1]), Ordering::Relaxed);
                sum_buffer[idx]
                    .2
                    .fetch_add(u64::from(pixel[2]), Ordering::Relaxed);
            });
    });
    println!("Average Picture processed");
    ImageBuffer::from_fn(width, height, |x, y| {
        let idx = (y * width + x) as usize;
        Rgb([
            (sum_buffer[idx].0.load(Ordering::Relaxed) / length) as u8,
            (sum_buffer[idx].1.load(Ordering::Relaxed) / length) as u8,
            (sum_buffer[idx].2.load(Ordering::Relaxed) / length) as u8,
        ])
    })
}

/// Subtracts one pixel value from another, with a minimum value of 0.
///
/// # Arguments
///
/// * `pixel1`: The first pixel value.
/// * `pixel2`: The second pixel value.
///
/// # Returns
///
/// The difference between the two pixel values, with a minimum value of 0.
fn pixel_substractor(pixel1: u8, pixel2: u8) -> u8 {
    if pixel1 >= pixel2 {
        255
    } else {
        pixel1 - pixel2
    }
}
/// Adds a "_processed" suffix to a file or directory path.
///
/// # Arguments
///
/// * `input`: The input path.
///
/// # Returns
///
/// The input path with "_processed" added to the filename or directory name.
fn pic_substractor(
    average_image: RgbImage,
    images: Vec<String>,
    source_path: &str,
    target_path: String,
) {
    let (width, height) = average_image.dimensions();
    images
        .into_par_iter()
        .map(|img| (img.clone(), path_to_rgb_image(source_path, &img).unwrap()))
        .for_each(|(path, img)| {
            // Check if the dimensions of both images are the same
            let mut result = RgbImage::new(width, height);
            (0..height).for_each(|y| {
                (0..width).for_each(|x| {
                    let pixel2 = average_image.get_pixel(x, y);
                    let pixel1 = img.get_pixel(x, y);

                    // Perform subtraction for each color channel
                    result.put_pixel(
                        x,
                        y,
                        Rgb([
                            pixel_substractor(pixel1[0], pixel2[0]),
                            pixel_substractor(pixel1[1], pixel2[1]),
                            pixel_substractor(pixel1[2], pixel2[2]),
                        ]),
                    );
                })
            });
            match result.save_with_format(
                format!("{}{}.Jpeg", target_path, path),
                image::ImageFormat::Jpeg,
            ) {
                Ok(_) => println!("{} saved", path),
                Err(e) => println!("{}", e),
            }
        });
}
fn pic_processing(folder_path: &str) {
    let processed_path: String =
        ensure_output_directory(&add_suffix(folder_path, "_processed").unwrap()).unwrap();
    let images: Vec<String> = read_images(folder_path, "jpg").expect("not found");
    average_pic(images.clone(), folder_path)
        .save_with_format(
            format!("{}average.jpg", processed_path),
            image::ImageFormat::Jpeg,
        )
        .unwrap();
    pic_substractor(
        average_pic(images.clone(), folder_path),
        images,
        folder_path,
        processed_path,
    );
}
fn main() {
    [
        "Gießen_Wieseck_1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-1",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-2",
        "Gießen_Wieseck_2/Gießen_Schwanenteich_1_2024_09_17-3",
    ]
    .into_iter()
    .for_each(|folder_path| pic_processing(format!("./data/{}/", folder_path).as_str()));
}
