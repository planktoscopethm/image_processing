mod preprocessing;
mod filtered_akaze;
mod feature_getting;
fn main() -> Result<(), Box<dyn std::error::Error>> {
 [
        "Gießen_Schwanenteich_1_24-09-05-1",
        "Gießen_Schwanenteich_1_24-09-05-2",
        "Gießen_Wieseck_1_24-09-05-1",
        "Gießen_Wieseck_1_24-09-05-2",
        "Gießen_Wieseck_1_24-09-05-3",
        "Gießen_Wieseck_1_24-09-05-4",
        "Gießen_Wieseck_1_24-09-05-5",
    ]
    .into_iter()
    .map(|folder|format!("./data/{}",folder))
    .for_each(|folder_path| preprocessing::pic_processing(format!("{}/", folder_path).as_str()))
    .for_each(|folder| filtered_akaze::filtered_akaze(format!("{}_processed/", folder).as_str()))
    .for_each(|path| feature_getting::akaze_feature(format!("{}_processed/", path).as_str()));
    Ok(())
}
