<img src="./pictures/ferris.png" alt="../pictures/ferris.png" height="100"/> <img src="./pictures/opencv.png" alt="../pictures/opencv.png" height="100"/>
# image_proc

This package provides tools for preprocessing image data specifically captured by the 
[THM](https://www.thm.de/site/en/) [planktoscope](https://www.planktoscope.org/). The goal is to transform 
raw images into a suitable format for efficient classification and subsequent machine learning analysis. 
Image processing is primarily performed using the [Rust](https://www.rust-lang.org/) native [image](https://crates.io/crates/image) 
library and [OpenCV](https://crates.io/crates/opencv).  

## getting started
To be able to use the software to its full extent, a few steps are necessary.

### Installation
* The Rust [installation](https://www.rust-lang.org/tools/install).
* The [OpenCV](https://opencv.org/) [installation](https://github.com/twistedfall/opencv-rust/blob/master/INSTALL.md)
Once the basics have been installed, the project must be installed. To do this, it must first be 
downloaded:
```sh
https://git.thm.de/planktoscopethm/image_processing.git
```
The project can then be installed with [cargo](https://doc.rust-lang.org/cargo/index.html):
```sh
cd image_processing
cargo build --release
```
The individual binary targets of the package can now be used:
```sh
cargo run --release --bin TARGET_NAME
```

